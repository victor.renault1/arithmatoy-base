#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

// Definition of a dynamic array
typedef struct {
    char *data; // data
    size_t size; // current size
    size_t capacity; // current capacity
} DynamicArray;

// Utility functions for addition, subtraction, and multiplication:

// Convert a character to its numeric value
unsigned int get_digit_value(char digit) {
    if (digit >= '0' && digit <= '9') {
        return digit - '0';
    }
    if (digit >= 'a' && digit <= 'z') {
        return 10 + (digit - 'a');
    }
    return -1;
}

// Convert a numeric value to its corresponding character
char to_digit(unsigned int value) {
    if (value >= ALL_DIGIT_COUNT) {
        debug_abort("Invalid value for to_digit()");
        return 0;
    }
    return get_all_digits()[value];
}

// Reverse a string in place
char *reverse(char *str) {
    const size_t length = strlen(str);
    const size_t limit = length / 2;
    for (size_t i = 0; i < limit; ++i) {
        char tmp = str[i];
        const size_t mirror = length - i - 1;
        str[i] = str[mirror];
        str[mirror] = tmp;
    }
    return str;
}

// Remove leading zeros from a number
const char *remove_leading_zeros(const char *number) {
    if (*number == '\0') {
        return number;
    }
    while (*number == '0') {
        ++number;
    }
    if (*number == '\0') {
        --number;
    }
    return number;
}

// Display a debug message and stop execution
void debug_abort(const char *debug_msg) {
    fprintf(stderr, "%s\n", debug_msg);
    exit(EXIT_FAILURE);
}

// Create a dynamic array
DynamicArray *create_dynamic_array() {
    DynamicArray *array = (DynamicArray *)malloc(sizeof(DynamicArray));
    array->data = NULL;
    array->size = 0;
    array->capacity = 0;
    return array;
}

// Add a value to the dynamic array
void add(DynamicArray *array, char value) {
    if (array->size >= array->capacity) {
        size_t new_capacity = array->capacity == 0 ? 1 : array->capacity * 2;
        char *new_data = (char *)realloc(array->data, new_capacity * sizeof(char));
        if (new_data == NULL) {
            fprintf(stderr, "Error: cannot allocate memory\n");
            exit(EXIT_FAILURE);
        }
        array->data = new_data;
        array->capacity = new_capacity;
    }
    array->data[array->size++] = value;
}

// Destroy a dynamic array
void destroy_dynamic_array(DynamicArray *array) {
    free(array->data);
    free(array);
}

// Get all possible digits (0-9, a-z)
const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

// Free memory allocated for a number
void arithmatoy_free(char *number) { free(number); }

// Get the reverse of a string
char *get_reverse(const char *to_reverse, size_t length) {
    char *reverse_str = calloc(length + 1, sizeof(char));
    memcpy(reverse_str, to_reverse, length * sizeof(char));
    return reverse(reverse_str);
}

// Compute the absolute value
int absolute_value(int value) {
    return value < 0 ? -value : value;
}

// Add two numbers in a given base
char *arithmatoy_add(unsigned int base, const char *left, const char *right) {
    if (VERBOSE) {
        fprintf(stderr, "add: entering function\n");
    }
    DynamicArray *result = create_dynamic_array();

    const char *charset = get_all_digits();

    const char *r_right = remove_leading_zeros(right);
    const char *r_left = remove_leading_zeros(left);
    size_t left_len = strlen(r_left);
    size_t right_len = strlen(r_right);
    size_t max = left_len > right_len ? left_len : right_len;
    char *reverse_right = get_reverse(r_right, right_len);
    char *reverse_left = get_reverse(r_left, left_len);

    char a, b;
    int carry = 0;
    char sum;

    for (size_t i = 0; i < max; i++) {
        a = left_len <= i ? '0' : reverse_left[i];
        b = right_len <= i ? '0' : reverse_right[i];
        sum = charset[(get_digit_value(a) + get_digit_value(b) + carry) % base];
        carry = (get_digit_value(a) + get_digit_value(b) + carry) / base;
        if (VERBOSE)
            fprintf(stderr, "add: digit %c digit %c carry %d\n", a, b, carry);
        add(result, sum);
    }

    while (carry != 0) {
        sum = charset[carry % base];
        carry = carry / base;
        add(result, sum);
    }

    add(result, '\0');
    size_t length = result->size;
    char *final_result = calloc(length, sizeof(char));
    memcpy(final_result, result->data, length * sizeof(char));
    destroy_dynamic_array(result);
    free(reverse_left);
    free(reverse_right);
    return reverse(final_result);
}

// Subtract two numbers in a given base (assuming left > right)
char *arithmatoy_subtract(unsigned int base, const char *left, const char *right) {
    if (VERBOSE) {
        fprintf(stderr, "subtract: entering function\n");
    }
    DynamicArray *result = create_dynamic_array();

    const char *charset = get_all_digits();

    const char *r_right = remove_leading_zeros(right);
    const char *r_left = remove_leading_zeros(left);
    size_t left_len = strlen(r_left);
    size_t right_len = strlen(r_right);
    size_t max = left_len > right_len ? left_len : right_len;
    char *reverse_right = get_reverse(r_right, right_len);
    char *reverse_left = get_reverse(r_left, left_len);

    char a, b;
    int carry = 0;
    int next_carry = 0;
    int intermediate;
    int index;
    char diff;

    for (size_t i = 0; i < max; i++) {
        a = left_len <= i ? '0' : reverse_left[i];
        b = right_len <= i ? '0' : reverse_right[i];
        intermediate = get_digit_value(a) - get_digit_value(b) - carry;
        if (intermediate < 0) {
            index = base - absolute_value(intermediate);
            next_carry = 1;
        } else {
            index = intermediate;
            next_carry = 0;
        }
        diff = charset[index % base];
        carry = next_carry;
        if (VERBOSE)
            fprintf(stderr, "subtract: digit %c digit %c carry %d\n", a, b, carry);
        add(result, diff);
    }

    add(result, '\0');
    size_t length = result->size;
    char *final_result = calloc(length, sizeof(char));
    memcpy(final_result, result->data, length * sizeof(char));
    destroy_dynamic_array(result);
    free(reverse_left);
    free(reverse_right);

    // Case where left < right
    if (carry > 0) {
        free(final_result);
        return NULL;
    }

    final_result = reverse(final_result);
    final_result = (char *)remove_leading_zeros((const char *)final_result);
    return final_result;
}

// Helper function to add multiple partial results
char *add_multiple(DynamicArray **results, size_t length, unsigned int base) {
    char *final_result;
    if (length == 1) {
        return results[0]->data;
    }
    final_result = results[0]->data;
    for (size_t i = 1; i < length; i++) {
        final_result = arithmatoy_add(base, (const char *)final_result, (const char *)results[i]->data);
    }
    return final_result;
}

// Compute the power of a number
int my_pow(int base, int exp) {
    if (exp == 0)
        return 1;
    int res = 1;
    for (int i = 0; i < exp; i++)
        res *= base;
    return res;
}

// Multiply two numbers in a given base
char *arithmatoy_multiply(unsigned int base, const char *left, const char *right) {
    if (VERBOSE) {
        fprintf(stderr, "multiply: entering function\n");
    }

    const char *charset = get_all_digits();

    const char *r_right = remove_leading_zeros(right);
    const char *r_left = remove_leading_zeros(left);
    size_t left_len = strlen(r_left);
    size_t right_len = strlen(r_right);
    char *reverse_right = get_reverse(r_right, right_len);
    char *reverse_left = get_reverse(r_left, left_len);

    DynamicArray **results = calloc(left_len * right_len, sizeof(DynamicArray *));
    size_t index = 0;
    for (size_t i = 0; i < left_len; i++) {
        for (size_t j = 0; j < right_len; j++) {
            DynamicArray *result = create_dynamic_array();
            int calc = get_digit_value(reverse_left[i]) * get_digit_value(reverse_right[j]);
            if (calc == 0) {
                add(result, '0');
            } else {
                calc *= my_pow(base, i + j);
                int res;
                while (calc != 0) {
                    res = calc % base;
                    calc /= base;
                    add(result, to_digit(res));
                }
                add(result, '\0');
                reverse(result->data);
            }
            results[index++] = result;
        }
    }

    char *final_result = add_multiple(results, left_len * right_len, base);

    char *end_result = calloc(strlen(final_result) + 1, sizeof(char));
    memcpy(end_result, final_result, strlen(final_result) * sizeof(char));

    for (size_t i = 0; i < left_len * right_len; i++) {
        destroy_dynamic_array(results[i]);
    }
    free(results);
    free(reverse_left);
    free(reverse_right);
    return end_result;
}
